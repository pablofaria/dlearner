# -*- coding: utf8 -*-

#
# Run all experiments.
#
# Uso: python experiments.py [cut-level]
#
# Global parameters
#
#    -fcoef : F-score coefficient value (default: 0.3)
#    -metric : spearman|cosine (default: spearman)
#


import sys, os
from nltk.tokenize import *

import time

# Model's scripts
from corpora import *      # Load corpora
from preprocess import *   # Preprocess corpora
from method import *       # Distributional learner

# Check for 'results' folder and create if necessary
if not os.path.exists('results'):
  os.makedirs('results')

# Print model info
print
print u"| Distributional Learner of Part-of-speech Categories"
print u"| A model for the study of the language acquisition process."
print u'|'
print u"| Main author: Pablo Faria, pablofaria@iel.unicamp.br"
print u'|'
print u"| Other participants: "
print u'|    Giulia Osaka (2017-2018)'
print
print u'Usage: python experiments.py [-fcoef=value] [-metric=value]'
print
print u'Global parameters:'
print
print u'    -fcoef : F-score coefficient value (default: 0.3)'
print u'    -metric : spearman|cosine (default: spearman)'
print 

# Load benchmark category data
print u'='*80
corpus_ref = generate_reftags()

# Constants
std_window = [-2,-1, 1, 2]
std_target = 1000
std_context = 150

# Show experiments list
def print_list_of_experiments():
  print u'='*80
  print u"Experiments:"
  print u"-"*80
  print u"0 - Standard"
  print u"1 - Varying context windows"
  print u"2 - Varying the number of target and context words"
  print u"3 - Performance per category"
  print u"4 - Varying corpus size"
  print u"5 - Different utterance boundaries"
  print u"6 - Frequency vs. Occurrence"
  print u"7 - Removing functional words"
  print u"8 - Contextual categorial information"
  print u"9 - Adult-to-adult speech"
  print u'='*80
    
while True:
  print_list_of_experiments()

  print u"Experiment [default=0, all='99', exit=-1]:",
  try:
    expr = int(raw_input())
  except:
    expr = 0
    
  if expr < 0: 
    break
  elif expr == 99:
    experiments = range(10)
  else:
    experiments = [expr]
 
  data = []
  if expr != 9:
    # Load child directed speech data (CEDAE, CHILDES)
    data.extend(load_cedae())    # list(tuples) : tuples = (id, text)
    data.extend(load_childes())  # list(tuples) : tuples = (id, text)

    # Convert 'data' to a list of tokens
    print u". Normalizing words..."
    data = [ normaliza(word.lower()) 
             for id, text in data 
             for s in sent_tokenize(text) 
             for word in word_tokenize(s) ]
             
  start = time.time()
 
  # Run experiments
  for expr in experiments:

    # Show dendrogram?
    flag_show = False
    try:
      # Don't ask for expr = 99 (see above)
      if len(experiments) == 1:
        flag_show = raw_input(u"Show dendrogram (y/n)? ")
        flag_show = flag_show.strip().lower() == 'y'
    except:
      pass

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Standard experiment (p.443) and experiment 3 (performance per word class)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr in [0,3]:

      print u'='*80
      if expr == 0:
        print u"Standard experiment..."
      else:
        print u"Experiment 3 (performance per word category)..."

      # Get target and context words
      twords, cwords = obtain_words(str(expr), data, corpus_ref, \
                                    std_target, std_context)

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, std_window)
    
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                   
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr), Z, twords, cluster_ref, 
                            sys.argv, flag_show)

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 1 (different context windows)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 1:

      print u'='*80
      
      # Get target and context words
      twords, cwords = obtain_words('1a', data, corpus_ref, \
                                    std_target, std_context)

      print u"Experiment 1a (window [1])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [1])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1a', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1b (window [2])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [2])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1b', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1c (window [3])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [3])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1c', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1d (window [4])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [4])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1d', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1e (window [-1])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-1])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1e', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1f (window [-2])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-2])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1f', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1g (window [-3])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-3])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1g', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1h (window [-4])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-4])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1h', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1i (window [-2,-1])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-2,-1])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1i', Z, twords, cluster_ref, sys.argv, flag_show)
    
      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1j (window [-1,1])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-1,1])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1j', Z, twords, cluster_ref, sys.argv, flag_show)
    
      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1k (window [1,2])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [1,2])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1k', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1l (window [-1,1,2])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-1,1,2])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1l', Z, twords, cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 1m (window [-2,-1,1])..."

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-2,-1,1])
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters('1m', Z, twords, cluster_ref, 
                            sys.argv, flag_show)


    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 2 (varying the number of target words)
    # (p.453-454)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 2:

      print u'='*80
      print u"Experiment 2a (varying the number of target words)..."
    
      # Quantities: [31, 100, 200, ..., 2000]
      number_of_words = [31, 100, 200, 300, 500, 1000, 1500, 2000]
      number_of_words.reverse()

      # Get target and context words
      twords_max, cwords = obtain_words(str(expr) + '_tw' + str(size), \
                                        data, corpus_ref, 2000, 150)

      for size in number_of_words:
        # Recorta a lista
        twords = twords_max[:size]

        print u".", size, "target words"
      
        # Pass 1: observation data
        cwindow, contig_table = build_contingency_table(
              data, twords, cwords, std_window)
  
        # Trim 'corpus_ref' using the benchmarked words
        cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                             if word in twords ])
                 
        # Pass 2
        X = prepare_observations(contig_table)
        Y = generate_distance_matrix(twords, X, sys.argv)

        # Pass 3
        Z = find_clusters(X, Y, normalization=True)

        # Pass 4
        T = evaluate_clusters(str(expr) + '_tw' + str(size), Z, twords, 
                              cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 2b (varying the number of context words)..."

      # Quantities: [10, 20, 30, 40, 50, 100, 150, 200, ... 500]
      number_of_words = [10, 20, 30, 40, 50, 100, 150, 200, 250, 350, 500]
      number_of_words.reverse()

      # Get target and context words
      twords, cwords_max = obtain_words(str(expr) + '_cw' + str(size), \
                                        data, corpus_ref, 1000, 500)

      for size in number_of_words:
        # Trim list
        cwords = cwords_max[:size]

        print u".", size, "context words"
    
        # Pass 1: observation data
        cwindow, contig_table = build_contingency_table(
              data, twords, cwords, std_window)
  
        # Trim 'corpus_ref' using the benchmarked words
        cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                             if word in twords ])
                 
        # Pass 2
        X = prepare_observations(contig_table)
        Y = generate_distance_matrix(twords, X, sys.argv)

        # Pass 3
        Z = find_clusters(X, Y, normalization=True)

        # Pass 4
        T = evaluate_clusters(str(expr) + '_cw' + str(size), Z, twords, cluster_ref, 
                              sys.argv, flag_show)


    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 3 (performance per word class)
    # [ SEE the standard experiment above ]
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 4 (corpus size)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 4:
      # size : [.25, .5, .75, 1] (original: 500k, 1000k, 1500k, 2000k)

      print u'='*80
      print u"Experiment 4 (varying the size of corpus)..."

      for p in [.1, .25, .5, .75, 1]:
    
        dataA = data[:int(round(p * len(data)))]
        print u".", p * 100, "% of the corpus (", len(dataA), "words)"
  
        # Get target and context words
        twords, cwords = obtain_words(str(expr) + '_p' + str(p), \
                                      dataA, corpus_ref, std_target, std_context)

        # Pass 1: observation data
        cwindow, contig_table = build_contingency_table(
              dataA, twords, cwords, std_window)
      
        # Trim 'corpus_ref' using the benchmarked words
        cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                             if word in twords ])
                     
        # Pass 2
        X = prepare_observations(contig_table)
        Y = generate_distance_matrix(twords, X, sys.argv)

        # Pass 3
        Z = find_clusters(X, Y, normalization=True)

        # Pass 4
        T = evaluate_clusters(str(expr) + '_p' + str(p), Z, twords, 
                              cluster_ref, sys.argv, flag_show)

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 5 (utterance boundaries)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 5:

      print u'='*80
      print u"Experiment 5a (one utterance at a time)..."
    
      dataA = data[:]
      dataB = data[:]

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'a', dataA, corpus_ref, \
                                    std_target, std_context, punct = 1)

      # Pass 1: observation data (utterance)
      cwindow, contig_table = build_contingency_table(
            dataA, twords, cwords, std_window, punct = 1)
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'a', Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 5b (5a + explicit final punctuation)..."

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'b', dataB, corpus_ref, \
                                    std_target, std_context, punct = 2)

      # Pass 1: observation data (utterance + explicit boundary)
      cwindow, contig_table = build_contingency_table(
            dataB, twords, cwords, std_window, punct = 2)
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'b', Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # Descarta dados gerados para este experimento
      dataA = None
      dataB = None
      

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 6: frequency vs. occurrence
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 6:

      print u'='*80
      print u"Experiment 6a (frequency + cityblock)..."

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'a', data, corpus_ref, \
                                    std_target, std_context)

      # Passo 1: medir a distribuição (enunciados separados)
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, std_window)
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Passo 2 (frequência + cityblock)
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv, \
                                   frequency = True, cityblock=True)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True, cityblock=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'a', Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 6b (occurrence + cityblock)..."

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'b', data, corpus_ref, \
                                    std_target, std_context)

      # Passo 1: medir a distribuição (enunciados separados)
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, std_window)
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table, frequency = False)
      Y = generate_distance_matrix(twords, X, sys.argv, \
                                   frequency = False, cityblock=True)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True, cityblock=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'b', Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 6c (occurrence + spearman)..."

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'c', data, corpus_ref, \
                                    std_target, std_context)

      # Passo 1: medir a distribuição (enunciados separados)
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, std_window)
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table, frequency = False)
      Y = generate_distance_matrix(twords, X, sys.argv, frequency = False)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'c', Z, twords, 
                            cluster_ref, sys.argv, flag_show)

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 7 (removing functional words)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 7:

      print u'='*80
      print u"Experiment 7 (removing functional words)..."

      # Remove itens funcionais do corpus
      dataA = [ w for w in data if w not in corpus_ref or corpus_ref[w] in \
                  ['noun','verb','adjective','adverb'] ]

      # Get target and context words
      twords, cwords = obtain_words(str(expr), dataA, corpus_ref, \
                                    std_target, std_context)

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            dataA, twords, cwords, std_window)
    
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                   
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr), Z, twords, 
                            cluster_ref, sys.argv, flag_show)


    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 8 (contextual categorial information)
    # :: Standard vs. Nominal cues vs. Verbal cues vs. Functional cues
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 8:

      print u'='*80
      print u"Experiment 8a (nominal cues: NOUN)..."

      # Remove itens funcionais do corpus
      dataA = []
      for w in data:
        try:
          if corpus_ref[w] == 'noun':
            dataA.append('wwwnoun')
            continue
        except:
          pass
        dataA.append(w)

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'a', dataA, corpus_ref, \
                                    std_target, std_context)

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            dataA, twords, cwords, std_window)
    
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                   
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'a', Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 8b (verbal cues: VERB)..."

      # Remove functional items
      dataA = []
      for w in data:
        try:
          if corpus_ref[w] == 'verb':
            dataA.append('wwwverb')
            continue
        except:
          pass
        dataA.append(w)

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'b', dataA, corpus_ref, \
                                    std_target, std_context)

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            dataA, twords, cwords, std_window)
    
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                   
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'b', Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # ++++++++++++++++++
      # ++++++++++++++++++
      print u"Experiment 8c (functional cues: FUNCTION)..."

      # Remove itens funcionais do corpus
      dataA = []
      for w in data:
        try:
          if corpus_ref[w] not in ['noun','verb','adverb','adjective']:
            dataA.append('wwwfunction')
            continue
        except:
          pass
        dataA.append(w)

      # Get target and context words
      twords, cwords = obtain_words(str(expr) + 'c', dataA, corpus_ref, \
                                    std_target, std_context)

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            dataA, twords, cwords, std_window)
    
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                   
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr) + 'c', Z, twords, cluster_ref, sys.argv, flag_show)


    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 9 (adult-to-adult speech)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 9:  

      print u'='*80
      print u"Experiment 9 (adult-to-adult speech)..."

      data = load_nurc()     # list(tuples) : tuples = (id, text)

      # Convert data into a list of tokens
      print u". Normalizing data..."
      data = [ normaliza(word.lower()) 
               for id, text in data 
               for s in sent_tokenize(text) 
               for word in word_tokenize(s) ]

      # Get target and context words
      twords, cwords = obtain_words(str(expr), data, corpus_ref, \
                                    std_target, std_context)

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, std_window)
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr), Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # Clean dataEsvazia os dados (para permitir outros experimentos)
      data = []

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Experiment 98 (best analysis for BP?)
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if expr == 98:  

      print u'='*80
      print u"Experiment 98 (best analysis for BP)..."

      data = load_nurc()     # list(tuples) : tuples = (id, text)

      # Convert data into a list of tokens
      print u". Normalizing data..."
      data = [ normaliza(word.lower()) 
               for id, text in data 
               for s in sent_tokenize(text) 
               for word in word_tokenize(s) ]

      # Get target and context words
      twords, cwords = obtain_words(str(expr), data, corpus_ref, \
                                    std_target, 100, punct = 2)

      # Pass 1: observation data
      cwindow, contig_table = build_contingency_table(
            data, twords, cwords, [-2,-1, 1], punct = 2)
      
      # Trim 'corpus_ref' using the benchmarked words
      cluster_ref = dict([ (word, cat) for word, cat in corpus_ref.items() \
                           if word in twords ])
                     
      # Pass 2
      X = prepare_observations(contig_table)
      Y = generate_distance_matrix(twords, X, sys.argv)

      # Pass 3
      Z = find_clusters(X, Y, normalization=True)

      # Pass 4
      T = evaluate_clusters(str(expr), Z, twords, 
                            cluster_ref, sys.argv, flag_show)

      # Clean dataEsvazia os dados (para permitir outros experimentos)
      data = []

  end = time.time()
  m, s = divmod(end - start, 60)
  h, m = divmod(m, 60)
  print u"[ Total running time:", "%d:%02d:%02d" % (h, m, s), "]"
  print "="*80
  print 

