# -*- coding: utf8 -*-

#
# This script will load each corpus used in the experiment.
#
#   NURC/RJ --> Policy: freely available.
#               URL: http://www.nurcrj.letras.ufrj.br/
#               Sections: "Diálogos entre informante e Documentador (DID)" and
#                         "Diálogos entre dois informantes (D2)"
#   CEDAE   --> Policy: freely available, but must be formally requested at
#       http://www3.iel.unicamp.br/cedae/faleconosco.php?view=form_atendimento
#               URL: 
#       http://eulalio.iel.unicamp.br/sys/audio/albums.php?action=show&album=18
#   CHILDES --> Policy: freely available.
#               URL:
#       https://childes.talkbank.org/browser/index.php?url=Romance/Portuguese/
#   TBC     --> Policy: freely available after registration.
#               URL: 
#       http://www.tycho.iel.unicamp.br/~tycho/corpus/texts/pos.zip
#

import os, re, nltk
from fnmatch import fnmatch
from nltk.tokenize import word_tokenize

# Preprocessing routines
from preprocess import *

# Base path for all corpora
root = 'corpora/v1/'

# Part-of-speech conversion table (from TBC to Redington et al.'s)
major_categories = {
	'N': 'noun',
	'NPR': 'noun',
	'ADJ': 'adjective',
	'OUTRO': 'adjective',
	'NUM': 'numeral',
	'VB': 'verb',
	'HV': 'verb',
	'ET': 'verb',
	'TR': 'verb',
	'SR': 'verb',
	'D': 'article',
	'CL': 'pronoun',
	'SE': 'pronoun',
	'DEM': 'pronoun',
	'PRO': 'pronoun',
	'PRO$': 'pronoun',
	'SENAO': 'pronoun',
	'QUE': 'pronoun',
	'WADV': 'pronoun',
	'WPRO': 'pronoun',
	'WD': 'pronoun',
	'WPRO$': 'pronoun',
	'WQ': 'pronoun',
	'ADV': 'adverb',
	'Q':  'adverb',
	'NEG': 'adverb', 
	'FP': 'adverb',
	'P': 'preposition',
	'CONJ': 'conjunction', 
	'CONJS': 'conjunction',
	'C': 'conjunction', 
	'INTJ': 'interjection'
	}

#
# Load the NURC/RJ corpus (adult-to-adult speech)
#
def load_nurc():
    path = root + 'NURC'    
    nurc_list = os.listdir(path)
    num=len(nurc_list) 
    duplas=[]
    total_words=[]
    totalsum_text=0

    print u"Loading NURC data..."
    for i in range(num):
        if not nurc_list[i].endswith('.txt'): continue

        # Load text
        texts = open(path+os.path.sep+nurc_list[i]).read().decode('utf8')  
        
        # Split on the main sections (metadata | speech data) 
        head, sep, tail = texts.partition('@start')                 
        texts = tail.strip()

        linhas = texts.split('\n')
        
        for actual_line in linhas:
            if actual_line.strip() != "":
                # Each line template: id . speech
                duplas_ID, sep, duplas_SPEECH = actual_line.partition(".")  
                datum = (duplas_ID, preproc_nurc(duplas_SPEECH))
                duplas.append(datum)

        texts = re.sub('LOC\.|DOC\.|LOC1\.|LOC2\.|DOC1\.|DOC2\.', '', texts) #NURC
        
    return duplas

#
# CEDAE acquisition data
#
def load_cedae():
    path = root + 'CEDAE'
    duplas = []
    total_words=[]
    totalsum_text=0

    print u'Loading CEDAE data...'
    for path, subdirs, files in os.walk(path):
        for name in files:
            if fnmatch(name, "*.txt"):
                texts = open(os.path.join(path, name)).read().decode('utf8')
                head, sep, texts = texts.partition('@Sessao')

                texts = preproc_cedae_global(texts.strip())
                
                linhas = texts.split('\n')
                
                for actual_line in linhas:
                    if actual_line.strip() != "":
                        # (ID .: SPEECH)
                        duplas_ID, sep, duplas_SPEECH = actual_line.partition(".:")                                  
                        datum = (duplas_ID, preproc_cedae(duplas_SPEECH))
                        if datum[1] == '' or datum[1] == '.': continue
                        duplas.append(datum)
                
    return duplas
    
#
# CHILDES acquisition data
#
def load_childes():
    path = root + 'CHILDES'
    duplas = []
    totalsum_text=0
    total_words=[]

    print u'Loading CHILDES data...'
    for path, subdirs, files in os.walk(path):
        for name in files:
            counter=0
            if fnmatch(name, "*.cha"):
                texts = open(os.path.join(path, name)).read().decode('utf8')
                texts = texts.strip()
                
                linhas = texts.split('\n')
                child_cod = []
                   
                for actual_line in linhas:                    
                    if actual_line.strip().startswith("@Participants:"):
                        child_cod.append(actual_line.strip()[15:18])
                        
                    if actual_line.strip() != "" and \
                            actual_line.strip().startswith("*") and \
                            actual_line.strip()[1:4] not in child_cod:
                            
                        duplas_ID, sep, duplas_SPEECH = actual_line.partition(":")
                        
                        speech_proc = preproc_childes(duplas_SPEECH.strip())
                        datum = (duplas_ID, speech_proc)
                        
                        if re.search(r'\w', datum[1]) is not None:
                            duplas.append(datum)                           

    return duplas


#
# Extraction of benchmark classification from a tagged corpus (TBC)
#
def generate_reftags():
    '''
    Generates the benchmark classification based on the Tycho Brahe Corpus (TBC). 
    Only the most frequent category is used (Redington et al., 1998:439).
    '''
    
    # Expected path
    path = root + 'benchmark'

    tychobrahe_list = os.listdir(path)
    num = len(tychobrahe_list) 
    wordlist_TB = []
    taglist_TB = []

    print u'Obtaining benchmark categorization...'
    
    for i in range(num):
        if not tychobrahe_list[i].endswith('.txt'): continue
        raw_text = open(path+os.path.sep+tychobrahe_list[i]).read().decode('utf8') 
       
        raw_text = re.sub('<.*?>', ' ', raw_text)
        raw_text = re.sub('(#!FORMAT=POS_0|/CODE)', '', raw_text)
        tag_word = raw_text.split()
        
        for j in xrange(len(tag_word)):
            WORD, sep, TAG = tag_word[j].partition("/") 
            WORD = WORD.lower()
            WORD = re.sub(r'^[\'@]|[\[\]]', '', WORD)
            TAG = re.sub(r'^P\+', '', TAG)   # contraction tags
            TAG = re.split(r'\-', TAG)[0]
            
            if re.match(r'^\*|[0-9]|[?!,]', WORD): continue
            if not TAG or \
                  re.match(r'^(CODE|ID|PONFP|FW|X+|W|WNP|WADVP|NP|ADJP|CONJP|QT|LATIN|PUNC)$|["?!,\.(]', TAG): 
              continue
            
            # TBC residual tagging errors
            TAG = TAG.replace('ADVR', 'ADV')
			
            wordlist_TB.append(WORD)
            try:
              taglist_TB.append(major_categories[TAG])
            except:
              taglist_TB.append(major_categories[re.split(r'[\+\-]', TAG)[0]])

    print u". TBC corpus:", len(wordlist_TB), u"word/tag pairs"
    print u". Calculating frequencies..."
    train_lists = nltk.ConditionalFreqDist(
                        (wordlist_TB[i], taglist_TB[i])
                        for i in range(len(wordlist_TB)))
    ref_cats = {}

    print u". Extracting most commom syntactic category for each word..."
    for cond, value in train_lists.items():
        try:
          ref_cats[cond] = value.most_common()[0][0]
        except:
          # Older NLTK version
          ref_cats[cond] = value.items()[0][0]
          
    file = open('results/ref_classes.txt','w') 
 
    for w in sorted(ref_cats.keys()):
      file.write((w + ', ' + ref_cats[w] + '\n').encode('utf8'))
 
    file.close() 

    return ref_cats

#        
# Evaluate the coverage of the benchmark classification for each 
# target words set.
#
def check_reftags_recall(corpus, reftags, number_of_twords=2000):
    ''' Check the coverage of the benchmark classification against 
    the list of target words. '''
    
    print u"Checking benchmark categorization coverage..."
    
    fdist = nltk.FreqDist(corpus)
    
    try:
      target_words = fdist.most_common(number_of_twords)
    except:
      # Older NLTK version
      target_words = fdist.items()[:number_of_twords]
      
    absent_words = []
    for pair in target_words:
        word, freq = pair
        try:
           eval('reftags[word]')
        except:
           absent_words.append(word)
           print u". Word not found [" + word + '/p' + \
                 str(target_words.index(pair)) + "/c" + str(freq) + "]"
                 
    cover_words = (float(len(target_words)-len(absent_words))/len(target_words))*100

    print u". Words not found:", len(absent_words)
    print u". Coverage:", cover_words
    
