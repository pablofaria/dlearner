# -*- coding: utf8 -*-

# General modules
import nltk, itertools, re

# Statistics
import numpy as np
import scipy, scipy.stats
import math, random
import sklearn

# Set fixed SEED for reproducibility of results
random.seed(571)

# Clustering and visualization
import scipy.cluster.hierarchy as hac
import matplotlib.pyplot as plt


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# PASS 1
# Build co-occurrence matrix
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def obtain_words(
        expr_name,
        corpus, 
        corpus_ref,    # Dictionary: word->tag
        number_of_twords = 1000, 
        number_of_cwords = 150,
        punct = 0
      ):
  '''
    Parameters:
      corpus     -> The data (list of tokens)
      corpus_ref -> Benchmark classification
      number_of_twords -> Number of target words
      number_of_cwords -> Number of context words
      punct -> 0: no final punctuation and utterances strung together (standard)
               1: no final punctuation, one utterance at a time
               2: final punctuation mark, one utterance at a time
  '''
  print u". Collecting", number_of_twords, "target words and", \
        number_of_cwords, "context words..."

  # Remove punctuation (standard experiment)
  if punct == 0:
    corpus = [ el for el in corpus if el != '.']
    # corpus = filter(lambda a: a != '.', x)
  
  # Get target and context words
  fdist = nltk.FreqDist(corpus)
  
  target_words = []
  context_words = []
  
  list_size = int(max(number_of_twords, number_of_cwords)*1.2)

  # Decreasing order of frequency
  if nltk.__version__ == '2.0.5':
    word_list = fdist.keys()[:list_size]
  else:
    word_list = [w for w, f in fdist.most_common()[:list_size] ]
    
  for word in word_list:
    if word in corpus_ref.keys():
      # ONLY valid words
      if re.match(r'^[bcdfghjlkmnpqrstvxzyw\-]$', word) is None:
        context_words.append(word)
        target_words.append(word)
    else:
      print u".. Word missing pos category [", word, "]"

  # Trim lists
  try:
    target_words.remove('.')  # never a target word (but maybe context)
  except:
    pass
  target_words = target_words[:number_of_twords]
  
  try:
    if punct != 2:
      context_words.remove('.')
  except:
    pass
  context_words = context_words[:number_of_cwords]

  print u'.. Tokens: ', len(corpus), \
        u'| Types: ', len(fdist.keys())
        
  rank_file = open("results/expr" + expr_name + "_word_rank.txt", "w")
  for i in range(len(target_words)):
    w = target_words[i]
    rank_file.write(('\t'.join([str(i+1), w, corpus_ref[w], \
                     str(fdist[w])]) + '\n').encode('utf8'))
  rank_file.close()           

  # Print categories and quantities
  fdist = nltk.ConditionalFreqDist(
              (corpus_ref[word], word)
              for word in target_words)
  for cond in fdist.conditions():
     print '....', cond.upper(), '(' + ', '.join(fdist[cond].keys()[:4]) + ')', \
           fdist[cond].N()
           
  return target_words, context_words

def build_contingency_table(
        corpus,
        target_words, 
        context_words, 
        context_window = [-1, 1],
        punct = 0
      ):
    '''
      NOTE: the name "contingency table" is due to Redington et al.'s own terms.
      
      Table of statistics:
        1st level: target words (alphabetical order)
        2nd level: positions (in the context window)
        3rd level: contextual words and their frequencies
                  
      Parameters:
        corpus -> A list of tokens
        target_words -> List of target words
        context_words -> List of context words
        context_window -> List of positions relative to the target word
        punct -> 0: no final punctuation and utterances strung together (standard)
                 1: no final punctuation, one utterance at a time
                 2: final punctuation mark, one utterance at a time
    '''
    print u". Generating co-occurrence matrix..."
    
    # Co-occurrence matrix: [target words][positions][context words]
    table = []
    for i in range(len(target_words)):
      table.append([ [ 0 for cw in context_words ] for pos in context_window ])
      
    sent_list = []
    if punct > 0:
      # Data as a list of sentences
      sent = []
      for el in corpus:
        if sent and el == '.':
          if punct == 2: sent.append(el)        
          sent_list.append(sent)
          sent = []
          continue
        
        sent.append(el)        
    else:
      # Data as a stream of sentences (punct = 0)
      sent_list.append(corpus)
      
    for sent in sent_list:
      for i in range(len(sent)):
          tword = sent[i]
          # For target word
          if tword in target_words:
              tword = target_words.index(tword)
              # Check context (positions)
              for k in range(len(context_window)):
                  j = context_window[k]
                  # For valid contextual items
                  if i+j >= 0 and i+j < len(sent) and sent[i+j] in context_words:
                      cword = context_words.index(sent[i+j])
                      # Count contextual word frequency
                      table[tword][k][cword] += 1
                        
    return context_window, table


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# PASS 2
# Compare distributions with "Spearman rank correlation coefficient"
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def prepare_observations(cooc_table, frequency=True):
  '''
    frequency -> True: frequency statistics | False: binary feature
  '''  
  print u". Preparing co-occurrence matrix..."
  
  # cooc_table: target words (lines) and observations (columns)
  X = []
  for i in range(len(cooc_table)):
    fullvet = []
    for vet in cooc_table[i]:
      fullvet.extend(vet)
  
    if frequency:
      X.append(fullvet)
    else:
      fullvet = [ int(el > 0) for el in fullvet ]
      X.append(fullvet)
      
  return X

def generate_distance_matrix(target_words, X, global_args, \
                             frequency=True, cityblock=False):    
  print u". Statistical comparison of target words..."

  # Global parameter: similarity measure can be 'spearman' or 'cosine'
  dist_metric = 'spearman'
  for arg in global_args:
    # Global parameter (if given)
    if arg.startswith('-metric='):
      try:
        dist_metric = arg[8:]
        print u".. Distance metric:", dist_metric
      except:
        pass
      break

  if not cityblock:
    if dist_metric == 'spearman':
      # axis=1 : lines are variables, columns are observations
      # Y[0] : correlation matrix (squared) | Y[1] : matrix of p-values
      Y = scipy.stats.spearmanr(X, axis=1)
    else:
      Y = [sklearn.metrics.pairwise.cosine_similarity(X), []]
      
    # Since the rank correlation between two vectors is in the range [-1,1] and 
    # negative distance is meaningless, we used an appropriate rescaling of 
    # values into the range [O,1]. (p.436)
    #
    # Note: It also applies to the 'cosine' measures [?] (ppff May-26-2019)
    #
    # Note 2: this normalization here seems unnecessary now that it is done to Z
    #         in the function 'find_clusters()' below. I'll leave it here for a
    #         while only until I make sure that it does not change results. 
    #         (May-27-2019)
    for i in range(len(Y[0])):
      Y[0][i] = [ remap(v, -1, 1, 0, 1) \
                  for v in Y[0][i] ]

    ret = np.nan_to_num(np.matrix(Y[0]))
        
  else:
    # Experiment 6 (p. 458) ('Y' is a condensed distance matrix)
    if frequency:
      Y = scipy.spatial.distance.pdist(X, metric='cityblock')
      # Normalization (values between 0 and 1)
      Y /= Y.max() 
    else:
      # cityblock = hamming distance for binary values
      Y = scipy.spatial.distance.pdist(X, metric='hamming')

    # Similarity: the inverse of dissimilarity
    Y = [ (1.0 - v) for v in Y ]
      
    # Make 'Y' squared
    Y = scipy.spatial.distance.squareform(Y)
    for i in range(len(Y)):
      Y[i][i] = 1.0   # Diagonal must be all 1.0s

    ret = np.matrix(Y)
 
  return ret

# Normalize values to the 0..1 interval
def remap(x, in_min, in_max, out_min, out_max):
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

    
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# PASS 3
# Cluster words by similarity using the "average linkage clustering" method
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def find_clusters(X, Y, normalization=False, cityblock=False):
  # NOTE (29-5-19): a warning is shown here when frequencies are used with cityblock
  # Why?
  print u". Finding clusters..."
  if cityblock:
    Z = hac.linkage(Y, 'average', 'cityblock')
  else:
   Z = hac.average(Y)
  
  # Always needed here, because the Z matrix has values outside the 0..1 interval
  if normalization:
    print u".. Normalizing Z matrix..."
    dists = [ el[2] for el in Z ]
    mind = min(dists)
    maxd = max(dists)
    for el in Z:
      el[2] = (el[2] - mind) / (maxd - mind)
  
  return Z


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# PASS 4
# Evaluate the method performance for various cut levels
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def evaluate_clusters(expr_id, Z, target_words, corpus_ref, global_args, flag_show):
  '''
  p.440: 'First, we "cut" the dendrogram at a range of levels of dissimilarity, obtaining a grouping of discrete sets of words. [...] the interesting information in the dendrogram is revealed when the dendrogram is cut at intermediate levels of dissimilarity, [...] We can evaluate the degree to which the benchmark syntactic categories are reflected in the dendrogram by considering how much the various groups obtained by cutting the den- drogram agree with the benchmark. To do this, we require some way of “scoring” the degree to which two groupings of the same items are in agreement.'
  
  p.441: 'The second kind of scoring that we used was information-theoretic. [...] This measure reflects both accuracy and completeness.'
  
  p.442: 'In the results presented below, we report accuracy, completeness, and when comparing classifications, informativeness, for clusterings at various levels of similarity.'
  
  p.442: 'In order to show that the method genuinely does provide useful information, we compare the method’s performance against a random baseline. For each level of similarity, we held the number of derived clusters, and the number of members of each cluster constant, but randomly assigned items to derived clusters, and then calculated accuracy, complete- ness, and informativeness as above. This means that random baselines must be calculated afresh for each different analysis. '
  
    p.450 (título da Figura 7): "For both accuracyand completeness, the lower
of the two lines is the random baseline, averaged over 10 random simulations."
    
  About measures (p.440):
    - acuracy: is the proportion of pairs of items which are grouped together in the derived groups which are also grouped together in the benchmark groups. 
    - completeness: is the proportion of pairs of items which are grouped by the benchmark which are also grouped together in the derived groupings.
    - informativiness (p.441): (Ii + Ij - Iij) / Iij
  '''
  
  print u". Finding the best cut level in the cluster hierarchy..."

  # Global parameter: similarity measure
  dist_metric = ''
  for arg in global_args:
    # Global parameter (if given)
    try:
      if arg.startswith('-metric=') and arg[8:] == 'cosine':
        dist_metric = '_cosine'
        break
    except:
      pass
  
  # Output files
  scoring_file = open("results/expr" + expr_id + "_scoring" + dist_metric + ".txt", "w")
  clusters_file = open("results/expr" + expr_id + "_clusters" + dist_metric + ".txt", "w")  
  
  best_f = 0.0
  best_prec = 0.0
  best_comp = 0.0
  best_cut = 0.0
  best_info = 0.0
  best_T = None
  best_cluster = None
  
  # 'beta' coefficient for the F-score
  # A value bigger than 2 favors completeness and less than 1 favors precision.
  # 
  # NOTE (Pablo): 0.3 seems to produce a good balance between precision and
  # completeness (at least for the standard experiment, with 1000 target words).
  # But this is something to discuss with statisticians.
  
  beta = 0.3
  for arg in global_args:
    # Global parameter (if given)
    if arg.startswith('-fcoef='):
      try:
        beta = float(arg[7:])
        print u".. F-score coefficient:", beta
      except:
        beta = 0.3
      break

  # Header  
  scoring_file.write(('\t'.join([u'Cut', u'Precision', 
      u'Completeness', u'F (b=' + str(beta) + ')', u'Prec (base)', 
      u'Comp (base)', u'F (base)']) + '\n').encode('utf8'))
        
  for cut_level in np.arange(0.0, 1.0, 0.01):
    # Obtain hierarchical clustering
    T = hac.fcluster(Z, cut_level, criterion='distance')
    
    # Generate test classification
    corpus_test = dict([ (target_words[i], T[i]) for i in range(len(T)) ])
    
    # Compare with the reference classification
    ref_set = set()
    test_set = set()
    for a, b in itertools.combinations(target_words, 2):
      # Target pairs
      if corpus_ref[a] == corpus_ref[b]: ref_set.add((a,b))
      # Test pairs
      if corpus_test[a] == corpus_test[b]: test_set.add((a,b))
   
    # TP is the number of true positives
    # FP is the number of false positives, and 
    # FN is the number of false negatives
    tp = test_set.intersection(ref_set)
    fp = test_set - tp
    fn = ref_set - tp

    # Precision (correct / guessed)
    prec = 0.0
    if test_set:
      prec = float(len(tp)) / len(test_set)

    # Completeness (correct / expected)
    comp = float(len(tp)) / len(ref_set)
  
    # F-score (see 'beta' above)
    F = 0.0
    if prec + comp > 0:
      F = (1 + beta**2) * (prec * comp) / (beta**2 * prec + comp)

    # Obtain the clustering with the best F
    if F > best_f or (F == best_f and len(set(corpus_test.values())) < best_T):
      best_f = F
      best_cut = cut_level
      best_T = len(set(corpus_test.values()))
      best_prec = prec
      best_comp = comp
      best_cluster = corpus_test

    # Generate baseline classification (cf. p.442 and p.450)
    
    base_f = 0
    base_prec = 0
    base_comp = 0
    for i in range(10):
      # Randomization
      corpus_base = randomize_groups(corpus_test)
      
      # Compare with the reference classification
      ref_setB = set()
      test_setB = set()
      for a, b in itertools.combinations(target_words, 2):
        # Target pairs
        if corpus_ref[a] == corpus_ref[b]: ref_setB.add((a,b))
        # Test pairs
        if corpus_base[a] == corpus_base[b]: test_setB.add((a,b))
   
      # TP is the number of true positives
      # FP is the number of false positives, and 
      # FN is the number of false negatives
      tpB = test_setB.intersection(ref_setB)
      fpB = test_setB - tpB
      fnB = ref_setB - tpB

      # Precision (correct / guessed)
      precB = 0.0
      if test_setB:
        precB = float(len(tpB)) / len(test_setB)

      # Completeness (correct / expected)
      compB = float(len(tpB)) / len(ref_setB)
  
      # F-score (see 'beta' above)
      FB = 0.0
      if precB + compB > 0:
        FB = (1 + beta**2) * (precB * compB) / (beta**2 * precB + compB)

      # Totals
      base_f += FB
      base_prec += precB
      base_comp += compB

    # Export measures
    scoring_file.write(('\t'.join([
        "{0:.2f}".format(cut_level), 
        "{0:.2f}".format(prec),
        "{0:.2f}".format(comp),
        "{0:.2f}".format(F),
        "{0:.2f}".format(base_prec / 10),
        "{0:.2f}".format(base_comp / 10),
        "{0:.2f}".format(base_f / 10)]) + '\n').replace('.',',').encode('utf8'))

  # Break measures by category (experiment 3) for the best cut level obtained above
  if expr_id[0] == '3':
    print u".. Assessing performance by pos category..."
    
    scoring_file_cat = open("results/expr" + expr_id + "_scoring_by_cat" + \
                            dist_metric + ".txt", "w")

    # Header  
    scoring_file_cat.write(('\t'.join([u'Category', u'Items', u'Precision', 
        u'Completeness', u'F (b=' + str(beta) + ')', u'Prec (base)', 
        u'Comp (base)', u'F (base)']) + '\n').encode('utf8'))
        
    categories = set(corpus_ref.values())
    words_by_cat = dict([ (cat, []) for cat in categories ])
    for word in target_words:
      words_by_cat[corpus_ref[word]].append(word)
      
    for cat, target_words_cat in words_by_cat.items():
    
      # Generate test classification
      corpus_test = best_cluster # dict([ (target_words[i], T[i]) for i in range(len(T)) ])
    
      # Compare with the reference classification
      tn = set()
      ref_set = set()
      test_set = set()
      for a, b in itertools.combinations(target_words, 2):
        # At least one of the two words must be of category 'cat'
        if not (a in target_words_cat or b in target_words_cat): continue
        
        # Target pairs
        if corpus_ref[a] == corpus_ref[b]: ref_set.add((a,b))
        # Test pairs
        if corpus_test[a] == corpus_test[b]:
          test_set.add((a,b))
        elif corpus_ref[a] != corpus_ref[b]: 
          # True negatives
          tn.add((a,b))
   
      # TP is the number of true positives
      # TN is the number of true negatives
      # FP is the number of false positives, and 
      # FN is the number of false negatives
      tp = test_set.intersection(ref_set)
      fp = test_set - tp
      fn = ref_set - tp

      # Accuracy = float(len(tp) + len(tn)) / (len(tp) + len(tn) + len(fp) + len(fn))

      # Precision (correct / guessed)
      prec = 0.0
      if test_set:
        prec = float(len(tp)) / len(test_set)
        
      # Completeness (correct / expected)
      comp = float(len(tp)) / len(ref_set)
  
      # F-score (see 'beta' above)
      F = 0.0
      if prec + comp > 0:
        F = (1 + beta**2) * (prec * comp) / (beta**2 * prec + comp)

      # Generate baseline classification (cf. p.442 and p.450)
    
      base_f = 0
      base_prec = 0
      base_comp = 0
      for i in range(10):
        # Randomization
        corpus_base = randomize_groups(corpus_test)
      
        # Compare with reference classification
        ref_setB = set()
        test_setB = set()
        for a, b in itertools.combinations(target_words, 2):
          # At least one of the two words must be of category 'cat'
          if not (a in target_words_cat or b in target_words_cat): continue
          
          # Target pairs
          if corpus_ref[a] == corpus_ref[b]: ref_setB.add((a,b))
          # Test pairs
          if corpus_base[a] == corpus_base[b]: test_setB.add((a,b))
   
        # TP is the number of true positives
        # FP is the number of false positives, and 
        # FN is the number of false negatives
        tpB = test_setB.intersection(ref_setB)
        fpB = test_setB - tpB
        fnB = ref_setB - tpB

        # Precision (correct / guessed)
        precB = 0.0
        if test_setB:
          precB = float(len(tpB)) / len(test_setB)

        # Completeness (correct / expected)
        compB = float(len(tpB)) / len(ref_setB)
  
        # F-score (see 'beta' above)
        FB = 0.0
        if precB + compB > 0:
          FB = (1 + beta**2) * (precB * compB) / (beta**2 * precB + compB)

        # Sum totals
        base_f += FB
        base_prec += precB
        base_comp += compB

      # Export measures to file
      scoring_file_cat.write(('\t'.join([
          cat,
          str(len(target_words_cat)), 
          "{0:.2f}".format(prec),
          "{0:.2f}".format(comp),
          "{0:.2f}".format(F),
          "{0:.2f}".format(base_prec / 10),
          "{0:.2f}".format(base_comp / 10),
          "{0:.2f}".format(base_f / 10)]) + '\n').replace('.',',').encode('utf8'))
 
    scoring_file_cat.close()

    # END of scoring by categories

  # Show best cut level scores
  print u'.. [best-F]', "{0:.2f}".format(best_f), \
        '(p:', "{0:.2f}".format(best_prec), \
        ', c:', "{0:.2f}".format(best_comp), \
        ') | C', "{0:.2f}".format(best_cut), '| G', best_T, '\n'

  if flag_show:      
    build_dendrogram(expr_id, Z, target_words, best_cut, best_T, flag_show)

  if best_cluster:
    # Export groups
    clusters = {}
    for word, group in best_cluster.items():
      try:
        clusters['g'+str(group)].append(word)
      except:
        clusters['g'+str(group)] = [word]
    
    clusters_file.write(u"# Clusters obtained for the best classification\n\n".encode('utf8'))
  
    for group, words in clusters.items():
      classes = {}
      for w in words:
        try:
          classes[corpus_ref[w]] += 1
        except:
          classes[corpus_ref[w]] = 1
      clusters_file.write(('[' + group + '] ' + ', '.join(words) + \
                           '\n{' + ', '.join([ k+'['+str(v)+']' \
                                 for k, v in classes.items() ]) + '} (' + \
                           str(len(words)) + ')\n\n').encode('utf8'))

    clusters_file.close()
  
  scoring_file.close()
  
  return best_cut
  
  
def randomize_groups(corpus):
  """ Randomize values in the 'corpus' dictionary (words as keys, classes as values). Classes are randomly reassigned to words. """
  
  random_corpus = {}

  items = corpus.items()
  word_list = [ k for k, v in items ]
  classes = [ v for k, v in items ]
  
  # Randomize
  random.shuffle(classes)
  
  # Reassign classes
  for i in range(len(word_list)):
    random_corpus[word_list[i]] = classes[i]
  
  return random_corpus


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# PASS 5
# Generate dendrogram (Sep/2019: not working very well; don't use it much.)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def build_dendrogram(expr_id, Z, target_words, cut_level=None, clusters=12, show=False):

  # D['leaves'] --> Target words (indices
  # D['color_list'] --> Colors (for cluster identification in the dendrogram)
  
  # General definitions
  plt.figure(1, figsize=(20, 8))
  plt.title(u'Hierarchical Clustering Dendrogram')
  plt.ylabel(u'target words')
  plt.xlabel(u'distance (cut: ' + str(cut_level) + ')')
  D = hac.dendrogram(
      Z,
      orientation='left',
      color_threshold=cut_level,
      get_leaves=True,
      leaf_rotation=90.,    # rotates the x axis labels
      leaf_font_size=8.,    # font size for the x axis labels
      labels=target_words,  # target words
  )  
  plt.axvline(x=cut_level, c='grey', lw=1, linestyle='dashed')

  plt.figure(2, figsize=(20, 8))
  plt.title(u'Hierarchical Clustering Dendrogram (truncated)')
  plt.ylabel(u'target words')
  plt.xlabel(u'distance (cut: ' + str(cut_level) + ')')
  hac.dendrogram(
      Z,
      orientation='left',
      truncate_mode='lastp',  # show only the last p merged clusters
      p=clusters,  # show only the last p merged clusters
      show_leaf_counts=True,  # otherwise numbers in brackets are counts
      leaf_rotation=90.,
      leaf_font_size=12.,
      show_contracted=True,  # to get a distribution impression in truncated branches
  )
  plt.show()
  
  return D
