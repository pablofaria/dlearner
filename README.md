# dlearner

Distributional Learner
Computational model of the learning of word categories through 
distributional analysis. 

-------------------------------------------------------------------------------
v1.0
Study based on Redington et al. (1998).
Main goal: reimplementation of Redington et al.'s (1998) study

Once datasets and the folder structure is set as explained below,
using this model should be straightforward (with default options):

  python -u experiments.py
  
Any problems/difficulties, please contact me on:

  pablofaria at iel dot unicamp dot br

Files included in this release:
  - The model source-code
  - The list of all files used as datasets (child directed speech, 
    adult-to-adult speech, and a reference tagged corpus)
  - An custom extension to the tagged corpus in the file "anexo_pos.txt"

* The actual corpora must be obtained from their sources, as indicated 
on the papers and in the source file "corpora.py". In this version, the
model is dependent on a fixed folder scheme: 'corpora/v1/' as the base
corpora folder with the following subfolders:

  - CEDAE ('CEDAE' datasets as ".txt" files)
  - CHILDES ('CHILDES' datasets as ".cha" files)
  - NURC ('NURC' datasets as ".txt" files)
  - benchmark (tagged datasets as ".txt" files)
    
Papers published upon this version:

  - Faria, P. e Ohashi, G. O. (2018). A aprendizagem distribucional no 
    português brasileiro: um estudo computacional. Revista LinguíStica, 
    14(3): 128–156.
  - Faria, P. (2019). The Role of Utterance Boundaries and Word 
    Frequencies for Part-of-speech Learning in Brazilian Portuguese 
    Through Distributional Analysis. In: Proceedings of the Workshop 
    on Cognitive Modeling and Computational Linguistics (NAACL’19), 
    152–159.
  
Features:
  - All 9 experiments implemented with some additions
  - Results are exported as output files to the folder 'results'
  - A command-line option to change the default F-score coefficient
  - A command-line option to alternate the vector similarity measure
    between 'spearman' and 'cosine'
  
Issues:
  - Redington et al.'s "informativeness" measure of performance is not 
    implemented; instead we're using the F-measure with a default 
    coefficient of 0.3
    
