# -*- coding: utf8 -*-

import re

'''
De Redington et al. (1998), p.438:

We did not clean up, or alter, the corpus in any way, apart from stripping away the CHILDES coding information, capitalization, and punctuation. The resulting corpus retained boundaries between utterances, but each utterance was an unpunctuated list of words. 

p.439: [...] different transcriptions of the same word were treated as completely separate words, [...] 
'''

# NOTE: The Zipf law predicts distinct frequencies for particular inflected forms of words (such as 'o/a/os/as' and 'aquele/a/es/as'). Should we lemmatize these items or not?

# For lower-cased tokens

normalizacoes = {

  # Ortography ----------------------

  # (vo)cê
  u'c' : u'cê',
  u'ce' : u'cê',            
  u'ceis' : u'cês',
  u'eli' : u'ele',
  u'êle' : u'ele',
  u'preli' : u'prele',

  # (es)tá
  u'táa' : u'tá',
  u'ta' : u'tá',
  u'tou' : u'tô',
  u'to' : u'tô',
  
  # p(a)ra
  u'prá' : u'pra',
  u'prás' : u'pras',
  u'prô' : u'pro',
  u'pru' : u'pro',

  # que (remove accent in "quê" and other normalizations)
  u'qu' : u'que',
  u'qui' : u'que',
  u'quê' : u'que',
  u'quee' : u'que',
  u'purquê' : u'porque',
  u'purque' : u'porque',
  u'porquê' : u'porque',
  
  u'pos' : u'pôs',
  
  u'du' : u'do',
  u'di' : u'de',
  u'aai' : u'ai',
  u'cum' : u'com',
  u'dêli' : u'dele',
  u'tém' : u'tem',
  u'dentru' : u'dentro',
  u'antis' : u'antes',
  u'mi' : u'me',
  u'la' : u'lá',
  u'dexa' : u'deixa',

  u'pô' : u'por',
  u'sê' : u'ser',
  u'tomá' : u'tomar',
  u'papá' : u'papar',
  u'buscá' : u'buscar',
  u'brincá' : u'brincar',
  u'ficá' : u'ficar',
  u'brincá' : u'brincar',
  u'ficá' : u'ficar', 
  u'falá' : u'falar', 
  u'pegá' : u'pegar', 
  u'tirá' : u'tirar', 
  u'levá' : u'levar', 
  u'contá' : u'contar', 
  u'guardá' : u'guardar', 
  u'comprá' : u'comprar', 
  u'passá' : u'passar', 
  u'ganhá' : u'ganhar', 
  u'entrá' : u'entrar', 
  u'botá' : u'botar', 
  u'deixá' : u'deixar', 
  u'sentá' : u'sentar', 
  u'conversá' : u'conversar', 
  u'chegá' : u'chegar', 
  u'jogá' : u'jogar', 
  u'mostrá' : u'mostrar', 
  u'lavá' : u'lavar', 
  u'nadá' : u'nadar', 
  u'arrumá' : u'arrumar', 
  u'pintá' : u'pintar',
  u'passeá' : u'passear',
  u'gravá' : u'gravar',
  u'cortá' : u'cortar',
  u'montá' : u'montar',
  u'andá' : u'andar',
  u'colá' : u'colar',
  u'olhá' : u'olhar',
  u'desenhá' : u'desenhar',
  u'esperá' : u'esperar',
  u'começá' : u'começar',
  u'almoçá' : u'almoçar',
  u'voltá' : u'voltar',
  u'deitá' : u'deitar',
  u'chorá' : u'chorar',
  u'trocá' : u'trocar',
  u'acabá' : u'acabar',
  u'apertá' : u'apertar',
  u'cantá' : u'cantar',
  u'limpá' : u'limpar',
  u'viajá' : u'viajar',
  u'ajudá' : u'ajudar',
  u'quebrá' : u'quebrar',
  u'formá' : u'formar',
  u'puxá' : u'puxar',
  u'usá' : u'usar',
  u'chamá' : u'chamar',
  u'colocá' : u'colocar',
  u'consertá' : u'consertar',
  u'trabalhá' : u'trabalhar',
  u'comê' : u'comer', 
  u'comé' : u'comer', 
  u'cumi' : u'comi',
  u'mexê' : u'mexer', 
  u'sabê' : u'saber',
  u'trazê' : u'trazer',
  u'batê' : u'bater',
  u'dizê' : u'dizer',
  u'descê' : u'descer',
  u'querê' : u'querer',
  u'qué' : u'quer',
  u'queru' : u'quero',
  u'escrevê' : u'escrever',
  u'falô' : u'falou', 
  u'ficô' : u'ficou', 
  u'cabô' : u'acabou', 
  u'acabô' : u'acabou',
  u'cabou' : u'acabou',
  u'levô' : u'levou',
  u'chegô' : u'chegou', 
  u'achô' : u'achou', 
  u'ganhô' : u'ganhou',
  u'pegô' : u'pegou',
  u'gostô' : u'gostou',
  u'tirô' : u'tirou',
  u'fechô' : u'fechou',
  u'contô' : u'contou',
  u'entrô' : u'entrou',
  u'passô' : u'passou',
  u'chorô' : u'chorou',
  u'quebrô' : u'quebrou',
  u'comprô' : u'comprou',
  u'deixô' : u'deixou',
  u'jogô' : u'jogou',
  u'dágua' : u"d'água", 
  u'memo' : u'mesmo',
  u'tuudo' : u'tudo',
  u'noossa' : u'nossa',
  u'naada' : u'nada',
  u'proonto' : u'pronto',
  u'intão' : u'então',
  u'oolha' : u'olha',
  u'issu' : u'isso',
  u'iisso' : u'isso',
  u'essi' : u'esse',
  u'prontu' : u'pronto',
  u'achu' : u'acho',
  u'ondi' : u'onde',
  u'genti' : u'gente',
  u'aondi' : u'aonde',
  u'pódi' : u'pode',
  u'vovozinha' : u'vovózinha',
  u'fazeno' : u'fazendo',
  u'quandu' : u'quando',
  u'vamu' : u'vamos', 
  u'vamo' : u'vamos',
  u'p~e' : u'põe',
  u'trazê' : u'trazer',
  u'estora' : u'estoura',
  u'dicar' : u'ficar',
  u'mamá' : u'mamar',
  u'fechá' : u'fechar',
  u'procurá' : u'procurar',
  u'acertô' : u'acertou',
  u'pulá' : u'pular',
  u'brigá' : u'brigar',
  u'dançá' : u'dançar',
  u'estragá' : u'estragar',
  u'ligá' : u'ligar',
  u'perguntá' : u'perguntar',
  u'tomô' : u'tomou',
  u'aprendê' : u'aprender',
  u'quisé' : u'quiser',
  u'achá' : u'achar',
  u'sarô' : u'sarou',
  u'crescê' : u'crescer',
  u'durmi' : u'dormir',
  u'durmiu' : u'dormiu',
  u'ensiná' : u'ensinar',
  u'parô' : u'parou',
  u'penteá' : u'pentear',
  u'perdê' : u'perder',
  u'acordá' : u'acordar',
  u'fazê' : u'fazer',

  u'mhhm' : u'hum',
  u'uhm' : u'hum',
  u'mmh' : u'hum',
  u'mh' : u'hum',
  u'hmm' : u'hum',
  u'hm' : u'hum',    

  u'hã' : u'ahn',
  u'hãm' : u'ahn',
  u'amh' : u'ahn',
  u'ãh' : u'ahn',
  u'ã' : u'ahn',

  u'aaah' : u'ah',
  u'aah' : u'ah',
  
  u'heim' : u'hein',
  u'hem' : u'hein',

  u'nã' : u'não', 
  u'nãao' : u'não', 
  u'nãão' : u'não',
  u'nuum' : u'num',
  u'éh' : u'é',     # interjection?
  u'ééé' : u'é',
  u'éé' : u'é', 
  u'è' : u'é',
  u'aquí' : u'aqui',
  u'ôi' : u'oi',
  u'óia' : u'olha',
  u'ói' : u'olha',
  u'ó' : u'olha',
  u'í' : u'aí',
  u'cç' : u'',
  u'ôpa' : u'opa',

  # "Semi-phonetic" transcriptions...
  u'u' : u'o',
  u'us' : u'os',
  u'nu' : u'no',
  u'mininus' : u'meninos',
  u'mininas' : u'meninas',
  u'minininha' : u'menininha',
  u'comu' : u'como',
  u'cumida' : u'comida',
  u'nomi' : u'nome',
  u'outru' : u'outro',
  u'outa' : u'outra',
  u'tamém' : u'também',

  u'manhê' : u'mãe',
  u'mame' : u'mamãe',
  u'mama' : u'mamãe',
  u'mamã' : u'mamãe',
  u'quedê' : u'cadê',
  u'péra' : u'espera',
  u'nenê' : u'neném',
  u'nenem' : u'neném',
  u'fóti' : u'forte',
  u'quiqui' : u'kiki',
  u'teté' : u'tete',
  u'tractor' : u'trator',
  u'bebé' : u'bebê',
  u'bebés' : u'bebês',
  u'chichi' : u'xixi',

  u'naná' : u'nana',
  u'agola' : u'agora',
  
  u'okay' : u'ok',

}

def normaliza(token):
  ret = token
  try:
    ret = normalizacoes[token]
  except:
    pass

  return ret
    
   
def preproc_childes(text):
  ''' Preprocessing for CHILDES data. '''
  
  patterns = [
    # CHILDES database coding marks
    ur'[0-9_]+',
    ur'\[[^\]]+\]',
    ur'[^ ]*@a', #1
    ur'[^ ]*@b',  #2
    ur'@c',  #3
    ur'@d',  #4
    ur'@e',  #5
    ur'@f',  #6
    ur'@g',  #7
    ur'[^ ]*@i',  #8
    ur'[^ ]*@l',  #10
    ur'@n',  #11
    ur'@o',  #12
    ur'[^ ]*@p',  #13
    ur'@q',  #14
    ur'@s\:[^\s]+',  #15
    ur'@s\$n',  #16
    ur'[^ ]*@si',  #17
    ur'[^ ]*@s\l',  #18
    ur'@sas',  #19
    ur'@t',  #20
    ur'@u',  #21
    ur'[^ ]*@wp',  #22
    ur'[^ ]*@x',  #23
    ur'@z\:[^\s]+',  #24
    ur'[^ ]*yyy',  #26
    ur'[^ ]*www',  #27
    ur'[^ ]*&',  #28
    (ur'\([^)]+\)', ''),    #29, 34 e 35  
    ur'0[^ ]*',  #30
    ur'[^ ]*\&\=',  #32
    ur'[^ ]*\&\*',  #33
    (ur'\(\.+\)', '.'), #34  [ppff: deixar '.']
    ur'\&*[^\*]*\*',  #36 e 37
    (ur'\+\.\.\.', '.'), #38
    (ur'\+\.\.\?', '?'), #39
    (ur'\+\!\?', '?!'), #40
    ur'\[\/\]',  #41
    (ur'\[\/\/\]', '.'),  #42
    (ur'\+\/\.', '.'),  #43
    (ur'\+\+', '.'), #44
    (ur'\/\-', '.'), #45
    (ur'\+\/\?', '?'), #46
    (ur'\+\/\/\.', '.'), #47
    (ur'\+\/\/\.', '.'), #48
    ur'\+\.',  #49
    ur'\+\"(\/)\.',  #51
    ur'\+\"',  #52
    ur'\+\"\.',  #52
    ur'\+\^',  #53
    ur'\+\,',  #54
    ur'\/\/',  
    ur'<',
    ur'>',
    (ur'_', ' '),
    (ur'([^=]+)=.+', r'\1'),
    ur'\=(riso|cantarola|canta|chora|tosse|ri|finge)',
    (r'\[\/\-\]', '.'), #69
    (r'\[\/\/\/\]', '.'), #68
    ur'\[*[^\]]*\]',  #55,56,58,59,60,62,63,64,65,66,70,71,72,73,74,75
    ur'\[\!\!\]|\[\!\]', #57

    ur'\*[^:]*?\:',    # *FALANTE.:

    # Punctuation and other
    ur'xxx+',
    (r'[\?\!;:]+', u'.'),      # All for '.'
    ur'[,"“”‘’–_„\@\+]',       # Remove intermediary punctuations
    (r'\s+\.', u'.'),          # ' .' -> '.'
    (r'\.+', u' . '),          # '...' -> '.'
    (r'\s+', u' '),            # Whitespace normalization
    ur'^\s*\.',                # Punctuation in the beginning of sentences
  ]

# Non treated cases:

#       Word without a reference tag [ ok ]
#       Word without a reference tag [ lastname ]
#       Word without a reference tag [ batatoon ]
#       Word without a reference tag [ sporting ]
#       Word without a reference tag [ biberon ]
#       Word without a reference tag [ gó ]  [?]
#       Word without a reference tag [ bah ]
#       Word without a reference tag [ bá ]   [ 'balinha' ]
#       Word without a reference tag [ miau ]
#       Word without a reference tag [ -eh ]  [ o que é '&-eh' ?]
#       Word without a reference tag [ cocó ]   [ galinha? ]
#       Word without a reference tag [ gagá ] [ ? ]
#       Word without a reference tag [ popó ] [ ? ]
#       Word without a reference tag [ totó ]  [ cachorro ]
#       Word without a reference tag [ cont ] [ conta, contou, ... ]
      
  for p in patterns:
    if type(p) != tuple:
      text = re.sub(p, ' ', text + ' ', flags=re.U)
    else:
      text = re.sub(p[0], p[1], text + ' ', flags=re.U)

  return text.strip()


def preproc_nurc(text):
  ''' Preprocessing for NURC data. '''

  patterns = [
    # Punctuation and other things
    (ur'[\/\\]', '.'),         # Interrupted speech
    (r'[\?\!;:]+', u'.'),      # All for '.'
     ur'[,"“”‘’\'`–_]',        # Remove intermediary punctuation
     ur'[\(\[][^\]\)]+[\]\)\}0]',   # Comments (from investigators)
     ur'[\(\)\[\]]',           # Inconsistent comments as valid text

    (r'\s+\.', u'.'),          # ' .' -> '.'
    (r'\.+', u' . '),          # '...' -> '.'
     ur'\-{2,}',               # Remove loose hyphens soltos when more than 2
    (r'\s+', u' '),            # Normalize whitespace    
     ur'^\s*\.',               # Remove punctuation in the beginning of the sentence
  ]

  for p in patterns:
    if type(p) != tuple:
      text = re.sub(p, ' ', text + ' ', flags=re.U)
    else:
      text = re.sub(p[0], p[1], text + ' ', flags=re.U)

  return text.strip()


def preproc_cedae_global(text):
  ''' Global preprocessing of CEDAE data. '''

  patterns = [
    # Inconsistencies in formatting
    (ur'Ad1:', '\nAd1.:'),
    (ur'Ad2:', '\nAd2.:'),
  ]
  
  for p in patterns:
    if type(p) != tuple:
      text = re.sub(p, ' ', text + ' ', flags=re.U)
    else:
      text = re.sub(p[0], p[1], text + ' ', flags=re.U)
      
  return text.strip()


def preproc_cedae(text):
  ''' Preprocessing of CEDAE data. '''

  # 1. Remove invisible characters
  # 2. Force a final dot in the end of every utterance
  text = text.strip() + '.'
  
  patterns = [
    # Transcription problems
    (ur't´\[a', u'tá'),
    (ur'djã', u'tchan'),
    (ur'á+', u'á'),
    (ur'ê+', u'ê'),
    (ur'å', u'ã'),
    (ur'eeu', u'e eu'),
    (ur'guardandopato', u'guardando pato'),
    (ur'hum-hum', u'hum hum'),
    (ur'SI', u''),

    # (Pseudo-)contractions
    (r'\s[Dd]\.\s', u' dona '),
    
    # Punctuation and others
    (ur'[\/\\]', '.'),         # Interrupted speech
    (r'[\?\!;:]+', u'.'),      # All for '.'
     ur'[,"“”‘’–_]',           # Remove intermediary punctuation
     ur'[\(\[][^\]\)]+[\]\)\}0]',   # Comments
     ur'[\(\)\[\]]',           # Inconsistent comments as valid text

    (r'\s+\.', u'.'),          # ' .' -> '.'
    (r'\.+', u' . '),          # '...' -> '.'
     ur'\-{2,}',               # Remove loose hyphens when more than 2
    (r'\s+', u' '),            # Normalize whitespace
     ur'^\s*\.',               # Remove punctuation in the beginning of sentences

    # Problems of Giulia's preparation
    (ur'[Dd] \. qui', u'daqui'),   # "Da" por "D.:"
    (ur'[Dd] \. i', u'dei'),   # "De" por "D.:"
    (ur'[Dd] \. dé', u'dedé'),   # "Dé" por "D.:"
    (ur'[Dd] \. du', u'dudu'),   # "Du" por "D.:"
    (ur'[Dd] \. dói', u'dodói'),   # "Do" por "D.:"
    (ur'[Mm] \. [Mm] \.', u'mama'),   # "Ma" por "M.:"
    (ur'[Mm] \. m', u'mam'),   # "Ma" por "M.:"
    (ur'[Mm] \. x', u'mex'),   # "Me" por "M.:"
    (ur'pr ([oaeu])', ur'pr\1'),  # preli, procê, ...
  ]

# Non treated cases:

#       Word without a reference tag [falcon]
#       Word without a reference tag [au]
#       Word without a reference tag [blim]
#       Word without a reference tag [inha] 
#       Word without a reference tag [có]   [chicken]
#       Word without a reference tag [tcho]
#       Word without a reference tag [ni]
#       Word without a reference tag [tchu]
#       Word without a reference tag [bi]
#       Word without a reference tag [pi]
#       Word without a reference tag [tan]  [onomatopéia]
#       Word without a reference tag [tchan]
#       Word without a reference tag [fa]  [musical note]
#       Word without a reference tag [ú]
#       Word without a reference tag [2]
#       Word without a reference tag [30]
#       Word without a reference tag [4]
#       Word without a reference tag [hmhm]
#       Word without a reference tag [tchi]
#       Word without a reference tag [bum]
#       Word without a reference tag [ok]
#       Word without a reference tag [mã]   [maybe 'mão' or 'mãe']
#       Word without a reference tag [tchic]
#       Word without a reference tag [aaa]
#       Word without a reference tag [tchô]   ['deixa eu' (=let me)]
#       Word without a reference tag [auau]
#       Word without a reference tag [miau]
#       Word without a reference tag [pr]   [Transcription errors]
#       Word without a reference tag [gugu]
#       Word without a reference tag [piu]
#       Word without a reference tag [daguia]

#       po, ca  ---> Ambiguous or unintelligible
#       hi --> laughings
#       ci --> unknown
#       aa --> unknown
#       i --> ambiguous
#       an --> noise
#       á --> ambiguous
            
  for p in patterns:
    if type(p) != tuple:
      text = re.sub(p, ' ', text + ' ', flags=re.U)
    else:
      text = re.sub(p[0], p[1], text + ' ', flags=re.U)

  return text.strip()

